===============================================================================
DESCRIPTION
===============================================================================

This is a saved game editor for Pokemon Emerald. It was created using the
saved game documentation found here:
http://bulbapedia.bulbagarden.net/wiki/Save_data_structure_in_Generation_III

===============================================================================
DOWNLOAD
===============================================================================

You can download an archive containing the runnable jar from the downloads
page. The archive also contains a copy of a pokemon emerald save file for
testing purposes. I did not include a rom and emulator.

===============================================================================
USAGE
===============================================================================

1) Double click the jar file.
2) Select a pokemon emerald save file to load by selecting File > Open.
3) Make changes.
4) Save a new file with the changes you made by selecting File > Save As...

===============================================================================
VIDEOS
===============================================================================

You can see me demonstrate the program here:
https://www.youtube.com/watch?v=-4HBVM8iS5s