package save_editing;

import java.util.ArrayList;

// game_model contains all the values that the save_parser will allow to
// be modified.
public class game_model
{
	ArrayList<pokemon>	_lineup;
	trainer				_trainer;
	
	public game_model()
	{
		_lineup = new ArrayList<pokemon>();
	}
	
	public game_model(game_model other)
	{
		_lineup = new ArrayList<pokemon>(other._lineup);
		_trainer = new trainer(other._trainer);
	}
	
	public ArrayList<pokemon> get_lineup()
	{
		return new ArrayList<pokemon>(_lineup);
	}
	
	public void set_lineup(ArrayList<pokemon> lineup)
	{
		_lineup = new ArrayList<pokemon>(lineup);
	}
	
	public trainer get_trainer()
	{
		return new trainer(_trainer);
	}
	
	public void set_trainer(trainer trainer)
	{
		_trainer = new trainer(trainer);
	}
}