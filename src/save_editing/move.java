package save_editing;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Scanner;

// Represents a pokemon's move.
public class move
{
	private int		_index;
	private short	_current_pp;
	private byte	_pp_bonuses_applied;
	
	private static HashMap<Integer, String> _move_index_to_name;
	
	static
	{
		_move_index_to_name = new HashMap<Integer, String>();
		_move_index_to_name.put(0, ""); // Lack of a move.
		
		try(Scanner file_in = new Scanner(new File("./moves.txt")))
		{
			file_in.useDelimiter(";");
			// Start at 1 because the first move is the lack of a move.
			for(int i = 1; file_in.hasNext(); ++i)
			{
				_move_index_to_name.put(i, file_in.next());
			}
		}
		catch(IOException ex)
		{
			System.out.println(ex);
		}
	}
	
	public move(int index, short current_pp, byte pp_bonuses_applied)
	{
		_index				= index;
		_current_pp			= current_pp;
		_pp_bonuses_applied = pp_bonuses_applied;
	}
	
	public static String index_to_move_name(int move_index)
	{
		return _move_index_to_name.get(move_index);
	}
	
	public int get_index() { return _index; }
	public void set_index(int index) { _index = index; }
	public short get_current_pp() { return _current_pp; }
	public void set_current_pp(short current_pp) { _current_pp = current_pp; }
	public byte get_pp_bonuses() {return _pp_bonuses_applied; }
	public void set_pp_bonuses(byte pp_bonuses) { _pp_bonuses_applied = pp_bonuses; }
}