package save_editing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;

// Represents the pokemon structure within save files. Read more here:
// http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_data_structure_in_Generation_III
class pokemon_structure
{	
	private final int _STRUCTURE_SIZE_BYTES = 100;
	private final int _NICKNAME_SIZE_BYTES = 10;
	private final int _OT_NAME_SIZE_BYTES = 7;
	private final int _DATA_SIZE_BYTES = 48;
	
	private ByteBuffer _structure;
	
	private int			_personality_value;
	private int			_ot_id;
	private String		_nickname;
	private short		_language;
	private String		_ot_name;
	private byte		_markings;
	private short		_checksum;
	private short		_unk;
	private byte[]		_data_raw;
	private ByteBuffer	_data;
	private int			_status_condition;
	private byte		_level;
	private byte		_pokerus_remaining;
	private short		_current_hp;
	private short		_total_hp;
	private short		_attack;
	private short		_defense;
	private short		_speed;
	private short		_sp_attack;
	private short		_sp_defense;
	
	private growth_substructure				_g;
	private attacks_substructure			_a;
	private evs_and_condition_substructure	_e;
	private miscellaneous_substructure		_m;
	
	private boolean	_data_decrypted;
	private int		_encryption_key;
	
	private static HashMap<Integer, String> _substructure_order;
	
	static
	{
		// The order of some substructures vary based on personality_value % 24.
		// This is a mapping of all different orders. Read about this here:
		// http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_data_substructures_in_Generation_III
		_substructure_order = new HashMap<Integer, String>();
		_substructure_order.put(1, "GAEM");
		_substructure_order.put(2, "GAME");
		_substructure_order.put(3, "GEAM");
		_substructure_order.put(4, "GEMA");
		_substructure_order.put(5, "GMAE");
		_substructure_order.put(6, "AGEM");
		_substructure_order.put(7, "AGME");
		_substructure_order.put(8, "AEGM");
		_substructure_order.put(9, "AEMG");
		_substructure_order.put(10, "AMGE");
		_substructure_order.put(11, "AMEG");
		_substructure_order.put(12, "EGAM");
		_substructure_order.put(13, "EGMA");
		_substructure_order.put(14, "EAGM");
		_substructure_order.put(15, "EAMG");
		_substructure_order.put(16, "EMGA");
		_substructure_order.put(17, "EMAG");
		_substructure_order.put(18, "MGAE");
		_substructure_order.put(19, "MGEA");
		_substructure_order.put(20, "MAGE");
		_substructure_order.put(21, "MAEG");
		_substructure_order.put(22, "MEGA");
		_substructure_order.put(23, "MEAG");
	}
	
	private void _read_member_variables()
	{
		_structure.position(0);
		
		_personality_value	= _structure.getInt();
		_ot_id				= _structure.getInt();
		
		byte[] nickname_pkmn = new byte[_NICKNAME_SIZE_BYTES];
		_structure.get(nickname_pkmn);
		_nickname			= string_conversion.to_java(nickname_pkmn);
		
		_language			= _structure.getShort();
		
		byte[] ot_name_pkmn = new byte[_OT_NAME_SIZE_BYTES];
		_structure.get(ot_name_pkmn);
		_ot_name			= string_conversion.to_java(ot_name_pkmn);
		
		_markings			= _structure.get();
		_checksum			= _structure.getShort();
		_unk				= _structure.getShort();
		
		_structure.get(_data_raw);
		_data				= ByteBuffer.wrap(_data_raw);
		_data.order(ByteOrder.LITTLE_ENDIAN);
		
		_status_condition	= _structure.getInt();
		_level				= _structure.get();
		_pokerus_remaining	= _structure.get();
		_current_hp			= _structure.getShort();
		_total_hp			= _structure.getShort();
		_attack				= _structure.getShort();
		_defense			= _structure.getShort();
		_speed				= _structure.getShort();
		_sp_attack			= _structure.getShort();
		_sp_defense			= _structure.getShort();
	}
	
	private void _write_member_variables()
	{
		_structure.position(0);
		
		_structure.putInt(_personality_value);
		_structure.putInt(_ot_id);
		
		byte[] nickname_pkmn = string_conversion.to_pkmn(_nickname, _NICKNAME_SIZE_BYTES);
		_structure.put(nickname_pkmn);
		
		_structure.putShort(_language);
		
		byte[] ot_name_pkmn = string_conversion.to_pkmn(_ot_name, _OT_NAME_SIZE_BYTES);
		_structure.put(ot_name_pkmn);
		
		_structure.put(_markings);
		_structure.putShort(_checksum);
		_structure.putShort(_unk);
		_structure.put(_data_raw);
		_structure.putInt(_status_condition);
		_structure.put(_level);
		_structure.put(_pokerus_remaining);
		_structure.putShort(_current_hp);
		_structure.putShort(_total_hp);
		_structure.putShort(_attack);
		_structure.putShort(_defense);
		_structure.putShort(_speed);
		_structure.putShort(_sp_attack);
		_structure.putShort(_sp_defense);
	}
	
	private void _decrypt_data()
	{
		assert !_data_decrypted : "The data is already decrypted.";
		
		_data.position(0);
		ByteBuffer data_put = _data.slice();
		data_put.order(ByteOrder.LITTLE_ENDIAN);
		
		int checksum = 0;
		while(0 < _data.remaining())
		{
			int decrypted_word = _data.getInt() ^ _encryption_key;
			checksum += (decrypted_word >>> 16) + (decrypted_word);
			data_put.putInt(decrypted_word);
		}
		
		_data_decrypted = true;
		
		assert (_checksum & 0x0000FFFF) == (checksum & 0x0000FFFF) : "The checksum is incorrect.";
	}
	
	private void _encrypt_data()
	{	
		assert _data_decrypted : "The data is already encrypted.";
	
		_data.position(0);
		ByteBuffer data_put = _data.slice();
		data_put.order(ByteOrder.LITTLE_ENDIAN);
		
		int checksum = 0;
		while(0 < _data.remaining())
		{
			int decrypted_word = _data.getInt();
			checksum += (decrypted_word >>> 16) + (decrypted_word);
			int encrypted_word = decrypted_word ^ _encryption_key;
			data_put.putInt(encrypted_word);
		}
		
		_checksum = (short)checksum;
	}
	
	private void _identify_substructures()
	{
		assert _data_decrypted : "Must decrypt before identifying substructures.";
		
		// The permutation of the substructures varies based on personality_value % 24. We
		// obtain that value, and then we instantiate the substructures in the proper order.
		_data.position(0);
		int substructure_order_index = (int)(Integer.toUnsignedLong(_personality_value) % 24);
		String initialization_order = _substructure_order.get(substructure_order_index);
		for(int i = 0; i < initialization_order.length(); ++i)
		{
			char identifier = initialization_order.charAt(i);
			switch(identifier)
			{
			case 'G':
				_g = new growth_substructure(_data);
				break;
			case 'A':
				_a = new attacks_substructure(_data);
				break;
			case 'M':
				_m = new miscellaneous_substructure(_data);
				break;
			case 'E':
				_e = new evs_and_condition_substructure(_data);
				break;
			}
		}
	}
	
	pokemon_structure(ByteBuffer pokemon_structure_data)
	{
		_structure = pokemon_structure_data.slice();
		pokemon_structure_data.position(pokemon_structure_data.position() + _STRUCTURE_SIZE_BYTES);
		_structure.order(ByteOrder.LITTLE_ENDIAN);
		_structure.limit(_STRUCTURE_SIZE_BYTES);
		
		_data_raw = new byte[_DATA_SIZE_BYTES];
		
		_data_decrypted = false;
		
		_read_member_variables();
		
		_encryption_key = _ot_id ^ _personality_value;
		
		if(!is_empty())
		{
			_decrypt_data();
			_identify_substructures();
		}
	}
	
	public void write_all()
	{
		if(_data_decrypted)
		{
			_g.write_all();
			_a.write_all();
			_m.write_all();
			_e.write_all();
			
			// Encrypt _AFTER_ writing the substructures.
			_encrypt_data();
		}
		_write_member_variables();
	}
	
	public move[] get_moves() 
	{
		move[] moves = new move[pokemon.get_max_moves()];
		
		short[] move_index = _a.get_moves();
		byte[] pp = _a.get_pp();
		byte[] pp_bonuses = _g.get_pp_bonuses();
		
		for(int i = 0; i < moves.length; ++i)
		{
			moves[i] = new move((int)move_index[i], (short)pp[i], (byte)pp_bonuses[i]);
		}
		
		return moves;
	}
	
	public void set_moves(move[] moves)
	{
		short[] move_index = new short[pokemon.get_max_moves()];
		byte[] pp = new byte[pokemon.get_max_moves()];
		byte[] pp_bonuses = new byte[pokemon.get_max_moves()];
		
		for(int i = 0; i < moves.length && i < pokemon.get_max_moves(); ++i)
		{
			move_index[i] = (short)moves[i].get_index();
			pp[i] = (byte)moves[i].get_current_pp();
			pp_bonuses[i] = (byte)moves[i].get_pp_bonuses();
		}
		
		_a.set_moves(move_index);
		_a.set_pp(pp);
		_g.set_pp_bonuses(pp_bonuses);
	}
	
	public boolean is_empty() { return _personality_value == 0; }
	public String get_nickname() { return _nickname; }
	public void set_nickname(String nickname) { _nickname = nickname; }
	public pokemon.individual_values get_ivs() { return _m.get_ivs(); }
	public void set_ivs(pokemon.individual_values ivs){ _m.set_ivs(ivs); }
	public pokemon.effort_values get_evs() { return _e.get_evs(); }
	public void set_evs(pokemon.effort_values evs) { _e.set_evs(evs); }
	public int get_personality_value() { return _personality_value; }
	public short get_level() { return _level; }
	public void set_level(short level){ _level = (byte)level; }
	public long get_experience() { return _g.get_experience(); }
	public void set_experience(long experience) { _g.set_experience(experience); }
}