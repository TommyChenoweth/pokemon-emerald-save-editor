package save_editing;

// Represents the pokemon structure within save files. Read more here:
// http://bulbapedia.bulbagarden.net/wiki/Save_data_structure_in_Generation_III#Section_0_-_trainer_info
class trainer_info_section extends section
{
	private final int _CHECKSUM_SIZE_BYTES = 3884;
	private final int _PLAYER_NAME_SIZE_BYTES = 7;
	
	private final int _PLAYER_NAME_OFFSET = 0x0000; // 7 Bytes
	private final int _PLAYER_GENDER_OFFSET = 0x0008; // 1 Byte
	private final int _TRAINER_ID_OFFSET = 0x000A; // 2 Bytes
	private final int _SECURITY_KEY_OFFSET = 0x00AC; // 4 Bytes
	private final int _SECRET_ID_OFFSET = 0x000C; // 2 Bytes

	private String	_player_name;
	private byte	_player_gender;
	private short	_trainer_id;
	private short	_secret_id;
	private int		_security_key;
	
	private void _read_player_name()
	{
		byte[] player_name_pkmn_str = new byte[_PLAYER_NAME_SIZE_BYTES];
		get_section_data().position(_PLAYER_NAME_OFFSET);
		get_section_data().get(player_name_pkmn_str);
		_player_name = string_conversion.to_java(player_name_pkmn_str);
	}
	
	private void _write_player_name()
	{
		byte[] player_name_pkmn = string_conversion.to_pkmn(_player_name, 7);
		get_section_data().position(_PLAYER_NAME_OFFSET);
		get_section_data().put(player_name_pkmn);
	}
	
	private void _read_player_gender()
	{
		get_section_data().position(_PLAYER_GENDER_OFFSET);
		_player_gender = get_section_data().get();
	}
	
	private void _write_player_gender()
	{
		get_section_data().position(_PLAYER_GENDER_OFFSET);
		get_section_data().put(_player_gender);
	}
	
	private void _read_trainer_id()
	{
		get_section_data().position(_TRAINER_ID_OFFSET);
		_trainer_id = get_section_data().getShort();
	}
	
	private void _write_trainer_id()
	{
		get_section_data().position(_TRAINER_ID_OFFSET);
		get_section_data().putShort(_trainer_id);
	}
	
	private void _read_secret_id()
	{
		get_section_data().position(_SECRET_ID_OFFSET);
		_secret_id = get_section_data().getShort();
	}
	
	private void _write_secret_id()
	{
		get_section_data().position(_SECRET_ID_OFFSET);
		get_section_data().putShort(_secret_id);
	}
	
	private void _read_security_key()
	{
		get_section_data().position(_SECURITY_KEY_OFFSET);
		_security_key = get_section_data().getInt();
	}
	
	public trainer_info_section(section s)
	{
		super(s);
		
		_read_player_name();
		_read_player_gender();
		_read_trainer_id();
		_read_secret_id();
		_read_security_key();
	}
	
	public void write_all()
	{
		_write_player_name();
		_write_player_gender();
		_write_trainer_id();
		_write_secret_id();
		
		// The order is important. The data must be written before the
		// checksum is computed.
		compute_and_write_checksum(_CHECKSUM_SIZE_BYTES);
	}
	
	public String get_player_name() { return _player_name; }
	public void set_player_name(final String player_name) { _player_name = player_name; }
	public trainer.gender get_player_gender() { return _player_gender == 0 ? trainer.gender.MALE : trainer.gender.FEMALE; }
	public void set_player_gender(trainer.gender gender) { _player_gender = gender == trainer.gender.MALE ? (byte)0 : (byte)1; }
	public int get_trainer_id() { return _trainer_id; }
	public void set_trainer_id(int trainer_id) { _trainer_id = (short)trainer_id; }
	public int get_secret_id() { return _secret_id; }
	public void set_secret_id(int secret_id) { _secret_id = (short)secret_id; }
	public int get_security_key() { return _security_key; }
}