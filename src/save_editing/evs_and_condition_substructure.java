package save_editing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// Represents the evs and condition substructure that's inside pokemon structures.
// Read more about substructures here:
// http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_data_substructures_in_Generation_III
class evs_and_condition_substructure
{
	private final int _SECTION_SIZE = 12;
	
	ByteBuffer _data;
	
	byte _hp_ev;
	byte _attack_ev;
	byte _defense_ev;
	byte _speed_ev;
	byte _sp_attack_ev;
	byte _sp_defense_ev;
	byte _coolness;
	byte _beauty;
	byte _cuteness;
	byte _smartness;
	byte _toughness;
	byte _feel;
	
	private void _read_member_variables()
	{
		_data.position(0);
		
		_hp_ev			= _data.get();
		_attack_ev		= _data.get();
		_defense_ev		= _data.get();
		_speed_ev		= _data.get();
		_sp_attack_ev	= _data.get();
		_sp_defense_ev	= _data.get();
		_coolness		= _data.get();
		_beauty			= _data.get();
		_cuteness		= _data.get();
		_smartness		= _data.get();
		_toughness		= _data.get();
		_feel			= _data.get();
	}
	
	private void _write_member_variables()
	{
		_data.position(0);
		
		_data.put(_hp_ev);
		_data.put(_attack_ev);
		_data.put(_defense_ev);
		_data.put(_speed_ev);
		_data.put(_sp_attack_ev);
		_data.put(_sp_defense_ev);
		_data.put(_coolness);
		_data.put(_beauty);
		_data.put(_cuteness);
		_data.put(_smartness);
		_data.put(_toughness);
		_data.put(_feel);
	}
	
	evs_and_condition_substructure(ByteBuffer structure)
	{
		_data = structure.slice();
		structure.position(structure.position() + _SECTION_SIZE);
		_data.order(ByteOrder.LITTLE_ENDIAN);
		_data.limit(_SECTION_SIZE);
		
		_read_member_variables();
	}
	
	public void write_all()
	{
		_write_member_variables();
	}
	
	public pokemon.effort_values get_evs()
	{
		pokemon.effort_values evs = new pokemon.effort_values();
		
		evs.hp			= _hp_ev;
		evs.attack		= _attack_ev;
		evs.defense		= _defense_ev;
		evs.speed		= _speed_ev;
		evs.sp_attack	= _sp_attack_ev;
		evs.sp_defense	= _sp_defense_ev;
		
		return evs;
	}
	
	public void set_evs(pokemon.effort_values evs)
	{
		_hp_ev			= evs.hp;
		_attack_ev		= evs.attack;
		_defense_ev		= evs.defense;
		_speed_ev		= evs.speed;
		_sp_attack_ev	= evs.sp_attack;
		_sp_defense_ev	= evs.sp_defense;
	}
}