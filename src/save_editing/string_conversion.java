package save_editing;

import java.util.HashMap;

// This is a class with methods for converting between the proprietary pokemon
// string format and java's unicode strings. // View the character encoding details here:
// http://bulbapedia.bulbagarden.net/wiki/Character_encoding_in_Generation_III
class string_conversion
{
	private static HashMap<Byte, Character> _pokemon_to_unicode_char;
	private final static char _java_string_terminator = '~';
	private final static char _java_placeholder_char = '?';
	
	private static HashMap<Character, Byte> _unicode_to_pokemon_char;
	private final static byte _pkmn_string_terminator = (byte)0xFF;
	private final static byte _pkmn_placeholder_char = (byte)0x00;
	
	static
	{
		// My table here is far from complete. A complete implementation would require
		// custom characters.
		_pokemon_to_unicode_char = new HashMap<Byte, Character>();
		
		_pokemon_to_unicode_char.put((byte)0x00, ' ');
		
		_pokemon_to_unicode_char.put((byte)0xA1, '0');
		_pokemon_to_unicode_char.put((byte)0xA2, '1');
		_pokemon_to_unicode_char.put((byte)0xA3, '2');
		_pokemon_to_unicode_char.put((byte)0xA4, '3');
		_pokemon_to_unicode_char.put((byte)0xA5, '4');
		_pokemon_to_unicode_char.put((byte)0xA6, '5');
		_pokemon_to_unicode_char.put((byte)0xA7, '6');
		_pokemon_to_unicode_char.put((byte)0xA8, '7');
		_pokemon_to_unicode_char.put((byte)0xA9, '8');
		_pokemon_to_unicode_char.put((byte)0xAA, '9');
		_pokemon_to_unicode_char.put((byte)0xAB, '!');
		_pokemon_to_unicode_char.put((byte)0xAC, '?');
		_pokemon_to_unicode_char.put((byte)0xAD, '.');
		_pokemon_to_unicode_char.put((byte)0xAE, '-');
		
		_pokemon_to_unicode_char.put((byte)0xBB, 'A');
		_pokemon_to_unicode_char.put((byte)0xBC, 'B');
		_pokemon_to_unicode_char.put((byte)0xBD, 'C');
		_pokemon_to_unicode_char.put((byte)0xBE, 'D');
		_pokemon_to_unicode_char.put((byte)0xBF, 'E');
		_pokemon_to_unicode_char.put((byte)0xC0, 'F');
		_pokemon_to_unicode_char.put((byte)0xC1, 'G');
		_pokemon_to_unicode_char.put((byte)0xC2, 'H');
		_pokemon_to_unicode_char.put((byte)0xC3, 'I');
		_pokemon_to_unicode_char.put((byte)0xC4, 'J');
		_pokemon_to_unicode_char.put((byte)0xC5, 'K');
		_pokemon_to_unicode_char.put((byte)0xC6, 'L');
		_pokemon_to_unicode_char.put((byte)0xC7, 'M');
		_pokemon_to_unicode_char.put((byte)0xC8, 'N');
		_pokemon_to_unicode_char.put((byte)0xC9, 'O');
		_pokemon_to_unicode_char.put((byte)0xCA, 'P');
		_pokemon_to_unicode_char.put((byte)0xCB, 'Q');
		_pokemon_to_unicode_char.put((byte)0xCC, 'R');
		_pokemon_to_unicode_char.put((byte)0xCD, 'S');
		_pokemon_to_unicode_char.put((byte)0xCE, 'T');
		_pokemon_to_unicode_char.put((byte)0xCF, 'U');
		_pokemon_to_unicode_char.put((byte)0xD0, 'V');
		_pokemon_to_unicode_char.put((byte)0xD1, 'W');
		_pokemon_to_unicode_char.put((byte)0xD2, 'X');
		_pokemon_to_unicode_char.put((byte)0xD3, 'Y');
		_pokemon_to_unicode_char.put((byte)0xD4, 'Z');
		
		_pokemon_to_unicode_char.put((byte)0xD5, 'a');
		_pokemon_to_unicode_char.put((byte)0xD6, 'b');
		_pokemon_to_unicode_char.put((byte)0xD7, 'c');
		_pokemon_to_unicode_char.put((byte)0xD8, 'd');
		_pokemon_to_unicode_char.put((byte)0xD9, 'e');
		_pokemon_to_unicode_char.put((byte)0xDA, 'f');
		_pokemon_to_unicode_char.put((byte)0xDB, 'g');
		_pokemon_to_unicode_char.put((byte)0xDC, 'h');
		_pokemon_to_unicode_char.put((byte)0xDD, 'i');
		_pokemon_to_unicode_char.put((byte)0xDE, 'j');
		_pokemon_to_unicode_char.put((byte)0xDF, 'k');
		_pokemon_to_unicode_char.put((byte)0xE0, 'l');
		_pokemon_to_unicode_char.put((byte)0xE1, 'm');
		_pokemon_to_unicode_char.put((byte)0xE2, 'n');
		_pokemon_to_unicode_char.put((byte)0xE3, 'o');
		_pokemon_to_unicode_char.put((byte)0xE4, 'p');
		_pokemon_to_unicode_char.put((byte)0xE5, 'q');
		_pokemon_to_unicode_char.put((byte)0xE6, 'r');
		_pokemon_to_unicode_char.put((byte)0xE7, 's');
		_pokemon_to_unicode_char.put((byte)0xE8, 't');
		_pokemon_to_unicode_char.put((byte)0xE9, 'u');
		_pokemon_to_unicode_char.put((byte)0xEA, 'v');
		_pokemon_to_unicode_char.put((byte)0xEB, 'w');
		_pokemon_to_unicode_char.put((byte)0xEC, 'x');
		_pokemon_to_unicode_char.put((byte)0xED, 'y');
		_pokemon_to_unicode_char.put((byte)0xEE, 'z');
		
		_pokemon_to_unicode_char.put((byte)0xFF, _java_string_terminator);
		
		_unicode_to_pokemon_char = new HashMap<Character, Byte>();
		_pokemon_to_unicode_char.forEach((b, c) -> { _unicode_to_pokemon_char.put(c, b); });
}
	
	// Convert a pokemon string to a java string.
	public static String to_java(final byte[] pkmn_string)
	{
		char[] unicode_string = new char[pkmn_string.length];
		
		for(int i = 0; i < pkmn_string.length; ++i)
		{
			Character unicode_character = _pokemon_to_unicode_char.get(pkmn_string[i]);
			unicode_string[i] = unicode_character != null ? unicode_character : _java_placeholder_char;
		}
		
		return new String(unicode_string);
	}
	
	// Convert a java string to a pokemon string.
	public static byte[] to_pkmn(final String java_string, final int pkmn_string_size)
	{
		byte[] pkmn_string = new byte[pkmn_string_size];
		
		for(int i = 0; i < pkmn_string_size; ++i)
		{
			Byte pkmn_character;
			
			// Unused characters are all terminators.
			pkmn_character = i < java_string.length() ? _unicode_to_pokemon_char.get(java_string.charAt(i)) : _pkmn_string_terminator;
			pkmn_string[i] = pkmn_character != null ? pkmn_character : _pkmn_placeholder_char;
		}
		
		return pkmn_string;
	}
}