package save_editing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// Represents the growth substructure that's inside pokemon structures.
// Read more about substructures here:
// http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_data_substructures_in_Generation_III
class growth_substructure
{
	private final int _SECTION_SIZE_BYTES = 12;
	private final int _PP_BONUS_BIT_MASK = 0x3;
	
	ByteBuffer _data;
	
	private short	_species;
	private short	_item_held;
	private int		_experience;
	private byte	_pp_bonuses;
	private byte	_friendship;
	private short	_unk;
	
	private void _read_member_variables()
	{
		_data.position(0);
		
		_species	= _data.getShort();
		_item_held	= _data.getShort();
		_experience	= _data.getInt();
		_pp_bonuses	= _data.get();
		_friendship	= _data.get();
		_unk		= _data.getShort();
	}
	
	private void _write_member_variables()
	{
		_data.position(0);
		
		_data.putShort(_species);
		_data.putShort(_item_held);
		_data.putInt(_experience);
		_data.put(_pp_bonuses);
		_data.put(_friendship);
		_data.putShort(_unk);
	}
	
	growth_substructure(ByteBuffer structure)
	{
		_data = structure.slice();
		structure.position(structure.position() + _SECTION_SIZE_BYTES);
		_data.order(ByteOrder.LITTLE_ENDIAN);
		_data.limit(_SECTION_SIZE_BYTES);
		
		_read_member_variables();
	}
	
	public void write_all()
	{
		_write_member_variables();
	}
	
	public byte[] get_pp_bonuses()
	{
		byte[] pp_bonuses = new byte[pokemon.get_max_moves()];
		
		for(int i = 0; i < pp_bonuses.length; ++i)
		{
			pp_bonuses[i] = (byte)(_pp_bonuses & (_PP_BONUS_BIT_MASK << (i * 2)) >>> (i * 2));
		}
		
		return pp_bonuses;
	}
	
	public void set_pp_bonuses(byte[] pp_bonuses)
	{
		byte new_pp_bonuses = 0;
		for(int i = 0; i < pp_bonuses.length && i < pokemon.get_max_moves(); ++i)
		{
			new_pp_bonuses |= ((pp_bonuses[i] & _PP_BONUS_BIT_MASK) << (i * 2));
		}
		
		_pp_bonuses = new_pp_bonuses;
	}
	
	public long get_experience() { return Integer.toUnsignedLong(_experience); }
	public void set_experience(long experience) { _experience = (int)experience; }
}