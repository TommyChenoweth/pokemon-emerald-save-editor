package save_editing;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// save_parser is responsible for reading and writing save files as well as
// parsing them to construct a game_model. The basic flow of use is this:
// read a save file -> get a game_model -> set a game_model -> write a new
// save file reflecting the changes made to game_model.
public class save_parser
{	
	// Offset to each save block in the save file.
	private final int _OFFSET_TO_SAVE_A = 0x0000;
	private final int _OFFSET_TO_SAVE_B = 0xE000;
	private final int _SAVE_BLOCK_SIZE = _OFFSET_TO_SAVE_B - _OFFSET_TO_SAVE_A;
	
	// Entire contents of the save file.
	private ByteBuffer _save_file_data;
	
	// This is the most recent of the two saves in the save file. This is the
	// one that the game will load, so this is the one that we want to make
	// changes to.
	private ByteBuffer _recent_save_block;
	
	// Sections that we care about.
	private trainer_info_section _tis;
	private team_and_items_section _tais;
	
	// Game values that support modification.
	private game_model _gm;
	
	// Construct a game model from the data read from the save file.
	public void _construct_game_model()
	{
		java.util.ArrayList<pokemon> lineup = new java.util.ArrayList<pokemon>();
		pokemon_structure[] lineup_structures = _tais.get_lineup_structures();
		for(pokemon_structure ps : lineup_structures)
		{
			if(ps.is_empty())
				break;
			
			pokemon p = new pokemon(ps.get_nickname(), ps.get_level(), ps.get_experience(), ps.get_moves(), ps.get_ivs(), ps.get_evs());
			lineup.add(p);
		}
		
		trainer t = new trainer(_tis.get_player_name(), _tis.get_player_gender(), _tis.get_trainer_id(), _tis.get_secret_id(), _tais.get_trainer_money());
		
		_gm = new game_model();
		_gm.set_lineup(lineup);
		_gm.set_trainer(t);
	}
	
	// Overwrite the saved game data with the data in the game model.
	public void _apply_game_model()
	{
		java.util.ArrayList<pokemon> lineup = _gm.get_lineup();
		pokemon_structure[] lineup_structures = _tais.get_lineup_structures();
		for(int i = 0; i < lineup.size(); ++i)
		{
			lineup_structures[i].set_nickname(lineup.get(i).get_nickname());
			lineup_structures[i].set_level(lineup.get(i).get_level());
			lineup_structures[i].set_experience(lineup.get(i).get_experience());
			lineup_structures[i].set_moves(lineup.get(i).get_moves());
			lineup_structures[i].set_ivs(lineup.get(i).get_ivs());
			lineup_structures[i].set_evs(lineup.get(i).get_evs());
		}
		
		trainer t = _gm.get_trainer();
		_tis.set_player_name(t.get_name());
		_tis.set_player_gender(t.get_gender());
		_tis.set_trainer_id(t.get_trainer_id());
		_tis.set_secret_id(t.get_secret_id());
		_tais.set_trainer_money(t.get_money());
	}
	
	private void _read_save_file(File save_file) throws IOException, FileTooBigException
	{
		final long file_size = save_file.length();
		
		// The max array size was taken from here:
		// http://stackoverflow.com/questions/4930114/why-declare-a-function-argument-to-be-final
		final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;
		if(MAX_ARRAY_SIZE < file_size)
		{
			// The file is too big to keep in a single array. There's no way that a
			// Pokemon Emerald save file should be this big.
			throw new FileTooBigException("The save file is too large! Ensure that your sav file is valid.");
		}
		
		byte[] file_buffer = new byte[(int)file_size];
		
		try(FileInputStream file_in = new FileInputStream(save_file))
		{
			file_in.read(file_buffer);
			_save_file_data = ByteBuffer.wrap(file_buffer);
			// Pokemon emerald save files are little endian.
			_save_file_data.order(ByteOrder.LITTLE_ENDIAN);
		}
		catch(IOException ex)
		{
			throw ex;
		}
	}
	
	private void _select_most_recent_save()
	{

		section sa = new section(_save_file_data);
		_save_file_data.position(_OFFSET_TO_SAVE_B);
		section sb = new section(_save_file_data);
		
		if(sa.get_save_index() < sb.get_save_index())
		{
			_save_file_data.position(_OFFSET_TO_SAVE_B);
			_recent_save_block = _save_file_data.slice();
		}
		else
		{
			_save_file_data.position(_OFFSET_TO_SAVE_A);
			_recent_save_block = _save_file_data.slice();
		}
		_recent_save_block.limit(_SAVE_BLOCK_SIZE);
		_recent_save_block.order(ByteOrder.LITTLE_ENDIAN);
	}
	
	private void _identify_sections()
	{
		while(0 < _recent_save_block.remaining())
		{
			section s = new section(_recent_save_block);
			switch(s.get_section_id())
			{
			case 0:
				System.out.println("Trainer Info: " + (_recent_save_block.position() - 0x1000));
				_tis = new trainer_info_section(s);
				break;
			case 1:
				System.out.println("Team and Items: " + (_recent_save_block.position() - 0x1000));
				_tais = new team_and_items_section(s);
				break;
			}
		}
		
		_tais.set_security_key_and_decrypt(_tis.get_security_key());
	}
	
	public void read_and_parse_save_file(final File save_file) throws IOException, FileTooBigException
	{
		_read_save_file(save_file);
		_select_most_recent_save();
		_identify_sections();
		_construct_game_model();
	}
	
	public void write_save_file(final File save_file) throws IOException
	{
		// Must apply the game model before writing section data.
		_apply_game_model();
		
		// Write section data.
		_tis.write_all();
		_tais.write_all();
		
		// Write the file.
		try(FileOutputStream file_out = new FileOutputStream(save_file, false))
		{
			file_out.write(_save_file_data.array());
		}
		catch(IOException ex)
		{
			throw ex;
		}
	}
	
	public game_model get_game_model()
	{
		return new game_model(_gm);
	}
	
	public void set_game_model(game_model gm)
	{
		_gm = gm;
	}
}