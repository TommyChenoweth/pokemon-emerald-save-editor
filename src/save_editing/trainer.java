package save_editing;

public class trainer
{
	public enum gender
	{
		MALE,
		FEMALE
	}
	
	private String	_name;
	private gender	_gender;
	private int		_trainer_id;
	private int		_secret_id;
	private int		_money;
	
	public trainer(String name, gender gender, int trainer_id, int secret_id, int money)
	{
		_name		= name;
		_gender		= gender;
		_trainer_id	= trainer_id;
		_secret_id	= secret_id;
		_money		= money;
	}
	
	public trainer(trainer other)
	{
		_name		= other._name;
		_gender		= other._gender;
		_trainer_id	= other._trainer_id;
		_secret_id	= other._secret_id;
		_money		= other._money;
	}
	
	public trainer()
	{
		_name		= "";
		_gender		= gender.MALE;
		_trainer_id	= 0;
		_secret_id	= 0;
		_money		= 0;
	}
	
	public String get_name() { return _name; };
	public void set_name(String name) { _name = name; }
	public gender get_gender() { return _gender; }
	public void set_gender(gender gender) { _gender = gender; }
	public int get_trainer_id() { return _trainer_id; }
	public void set_trainer_id(int trainer_id) { _trainer_id = trainer_id; }
	public int get_secret_id() { return _secret_id; }
	public void set_secret_id(int secret_id) { _secret_id = secret_id; }
	public int get_money() { return _money; }
	public void set_money(int money) { _money = money; }
}