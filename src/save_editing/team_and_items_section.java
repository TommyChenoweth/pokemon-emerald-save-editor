package save_editing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

//Represents the team and items structure within save files. Read more here:
//http://bulbapedia.bulbagarden.net/wiki/Save_data_structure_in_Generation_III#Section_0_-_trainer_info
class team_and_items_section extends section
{
	private final int _CHECKSUM_SIZE_BYTES = 3968;
	private final int _TEAM_SIZE_BYTES = 600;
	
	private final int _TEAM_SIZE_OFFSET = 0x0234; // 4 Bytes
	private final int _TEAM_POKEMON_LIST_OFFSET = 0x0238; // 600 Bytes
	private final int _MONEY_OFFSET = 0x0490; // 4 Bytes
	
	private int					_team_size;
	private ByteBuffer			_team_data;
	private pokemon_structure[]	_lineup;
	private int					_money;
	private int					_security_key;
	
	private boolean _data_decrypted;

	private void _read_team_size()
	{
		get_section_data().position(_TEAM_SIZE_OFFSET);
		_team_size = get_section_data().getInt();
	}
	
	private void _write_team_size()
	{
		get_section_data().position(_TEAM_SIZE_OFFSET);
		get_section_data().putInt(_team_size);
	}
	
	private void _read_team_pokemon_list()
	{
		get_section_data().position(_TEAM_POKEMON_LIST_OFFSET);
		_team_data = get_section_data().slice();
		_team_data.order(ByteOrder.LITTLE_ENDIAN);
		_team_data.limit(_TEAM_SIZE_BYTES);
	}
	
	private void _write_team_pokemon_list()
	{
		for(pokemon_structure ps : _lineup)
		{
			ps.write_all();
		}
	}
	
	private void _read_money()
	{
		get_section_data().position(_MONEY_OFFSET);
		_money = get_section_data().getInt();
	}
	
	private void _write_money()
	{
		get_section_data().position(_MONEY_OFFSET);
		int encrypted_money = _data_decrypted ? _money ^ _security_key :  _money;
		get_section_data().putInt(encrypted_money);
	}
	
	private void _decrypt_section_data()
	{
		_money ^= _security_key;
		
		_data_decrypted = true;
	}
	
	private void _decrypt_pokemon_data()
	{
		_lineup = new pokemon_structure[6];
		_team_data.position(0);
		for(int i = 0; i < _lineup.length; ++i)
		{
			pokemon_structure ps = new pokemon_structure(_team_data);
			if(!ps.is_empty())
			{
				System.out.println("Nickname: " + ps.get_nickname());
			}

			_lineup[i] = ps;
		}
	}
	
	public team_and_items_section(section s)
	{
		super(s);
		
		_data_decrypted = false;
		
		_read_team_size();
		_read_team_pokemon_list();
		_read_money();
		
		_decrypt_pokemon_data();
	}
	
	public void write_all()
	{
		_write_team_size();
		_write_team_pokemon_list();
		_write_money();
		
		// The order is important. The data must be written before the
		// checksum is computed.
		compute_and_write_checksum(_CHECKSUM_SIZE_BYTES);
	}
	
	public void set_security_key_and_decrypt(final int security_key)
	{
		_security_key = security_key;
		_decrypt_section_data();
	}
	
	public pokemon_structure[] get_lineup_structures()
	{
		return _lineup;
	}
	
	public int get_trainer_money() { return _money; };
	public void set_trainer_money(int money) { _money = money; }
}