package save_editing;

public class FileTooBigException extends Exception
{
	public FileTooBigException(String message)
	{
		super(message);
	}
	
	public FileTooBigException(String message, Throwable throwable)
	{
		super(message, throwable);
	}
}