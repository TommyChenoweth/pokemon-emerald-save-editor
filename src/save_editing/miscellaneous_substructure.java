package save_editing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

// Represents the miscellaneous substructure that's inside pokemon structures.
// Read more about substructures here:
// http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_data_substructures_in_Generation_III
class miscellaneous_substructure
{
	private final int _SECTION_SIZE = 12;
	ByteBuffer _data;
	
	private final int _IV_BIT_MASK = 0x1F;
	private final int _EGG_BIT_MASK = 0x1 << 30;
	private final int _ABILITY_BIT_MASK = 0x1 << 31;
	
	private byte _pokerus_status;
	private byte _met_location;
	private short _origins_info;
	private int _iv_egg_ability;
	private int _ribbons_obedience;
	
	private void _read_member_variables()
	{
		_data.position(0);
		_pokerus_status = _data.get();
		_met_location = _data.get();
		_origins_info = _data.getShort();
		_iv_egg_ability = _data.getInt();
		_ribbons_obedience = _data.getInt();
	}
	
	private void _write_member_variables()
	{
		_data.position(0);
		_data.put(_pokerus_status);
		_data.put(_met_location);
		_data.putShort(_origins_info);
		_data.putInt(_iv_egg_ability);
		_data.putInt(_ribbons_obedience);
	}
	
	miscellaneous_substructure(ByteBuffer structure)
	{
		_data = structure.slice();
		structure.position(structure.position() + _SECTION_SIZE);
		_data.order(ByteOrder.LITTLE_ENDIAN);
		_data.limit(_SECTION_SIZE);
		
		_read_member_variables();
	}
	
	public void write_all()
	{
		_write_member_variables();
	}
	
	public pokemon.individual_values get_ivs()
	{
		pokemon.individual_values ivs = new pokemon.individual_values();
		
		ivs.hp			= (byte)(((_IV_BIT_MASK << 0)  & _iv_egg_ability) >> 0);
		ivs.attack		= (byte)(((_IV_BIT_MASK << 5)  & _iv_egg_ability) >> 5);
		ivs.defense 	= (byte)(((_IV_BIT_MASK << 10) & _iv_egg_ability) >> 10);
		ivs.speed		= (byte)(((_IV_BIT_MASK << 15) & _iv_egg_ability) >> 15);
		ivs.sp_attack	= (byte)(((_IV_BIT_MASK << 20) & _iv_egg_ability) >> 20);
		ivs.sp_defense	= (byte)(((_IV_BIT_MASK << 25) & _iv_egg_ability) >> 25);
		
		return ivs;
	}
	
	public void set_ivs(pokemon.individual_values ivs)
	{
		int new_ivs = 0;
		
		new_ivs |= (ivs.hp & _IV_BIT_MASK) << 0;
		new_ivs |= (ivs.attack & _IV_BIT_MASK) << 5;
		new_ivs |= (ivs.defense & _IV_BIT_MASK) << 10;
		new_ivs |= (ivs.speed & _IV_BIT_MASK) << 15;
		new_ivs |= (ivs.sp_attack & _IV_BIT_MASK) << 20;
		new_ivs |= (ivs.sp_defense & _IV_BIT_MASK) << 25;
		
		final int EGG_ABILITY_MASK = _EGG_BIT_MASK | _ABILITY_BIT_MASK;
		_iv_egg_ability = new_ivs | (_iv_egg_ability & EGG_ABILITY_MASK);
	}
}