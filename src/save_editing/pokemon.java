package save_editing;

public class pokemon
{
	public enum gender
	{
		MALE,
		FEMALE
	}
	
	public enum nature
	{
		HARDY,
		LONELY,
		BRAVE,
		ADAMANT,
		NAUGHTY,
		BOLD,
		DOCILE,
		RELAXED,
		IMPISH,
		LAX,
		TIMID,
		HASTY,
		SERIOUS,
		JOLLY,
		NAIVE,
		MODEST,
		MILD,
		QUIET,
		BASHFUL,
		RASH,
		CALM,
		GENTLE,
		SASSY,
		CAREFUL,
		QUIRKY
	}
	
	public static class individual_values
	{
		public byte hp;
		public byte attack;
		public byte defense;
		public byte speed;
		public byte sp_attack;
		public byte sp_defense;
	}
	
	public static class effort_values
	{
		public byte hp;
		public byte attack;
		public byte defense;
		public byte speed;
		public byte sp_attack;
		public byte sp_defense;
	}
	
	private static final int _MAX_MOVES = 4;
	
	private String				_nickname;
	private short				_level;
	private long				_experience;
	private move[]				_moves;
	private individual_values	_ivs;
	private effort_values		_evs;
	
	public pokemon()
	{
		_moves = new move[_MAX_MOVES];
	}
	
	public pokemon(String nickname, short level, long experience, move[] moves, individual_values ivs, effort_values evs)
	{
		this();
		
		_nickname	= nickname;
		_level		= level;
		_experience	= experience;
		_moves		= moves;
		_ivs		= ivs;
		_evs		= evs;
	}
	
	public static int get_max_moves()
	{
		return _MAX_MOVES;
	}
	
	public String get_nickname() { return _nickname; }
	public void set_nickname(String nickname) { _nickname = nickname; }
	public short get_level() { return _level; }
	public void set_level(short level) { _level = level; }
	public long get_experience() { return _experience; }
	public void set_experience(long experience) { _experience = experience; }
	public move[] get_moves() { return _moves; }
	public void set_moves(move[] moves) { _moves = moves; }
	public individual_values get_ivs() { return _ivs; }
	public void set_ivs(individual_values ivs) { _ivs = ivs; }
	public effort_values get_evs() { return _evs; }
	public void set_evs(effort_values evs) { _evs = evs; }
}