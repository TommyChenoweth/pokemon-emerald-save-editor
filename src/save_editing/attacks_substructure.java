package save_editing;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

// Represents the attack substructure that's inside pokemon structures.
// Read more about substructures here:
// http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_data_substructures_in_Generation_III
class attacks_substructure
{
	private final int _SECTION_SIZE = 12;
	
	ByteBuffer _data;
	
	short[]	_moves;
	byte[]	_pp;
	
	private void _read_member_variables()
	{
		_data.position(0);
		
		for(int i = 0; i < _moves.length; ++i)
		{
			_moves[i] = _data.getShort();
		}
		for(int i = 0; i < _pp.length; ++i)
		{
			_pp[i] = _data.get();
		}
	}
	
	private void _write_member_variables()
	{
		_data.position(0);
		
		for(short move : _moves)
		{
			_data.putShort(move);
		}
		for(byte pp : _pp)
		{
			_data.put(pp);
		}
	}
	
	attacks_substructure(ByteBuffer structure)
	{
		_data = structure.slice();
		structure.position(structure.position() + _SECTION_SIZE);
		_data.order(ByteOrder.LITTLE_ENDIAN);
		_data.limit(_SECTION_SIZE);
		
		_moves = new short[pokemon.get_max_moves()];
		_pp = new byte[pokemon.get_max_moves()];
		
		_read_member_variables();
	}
	
	public void write_all()
	{
		_write_member_variables();
	}
	
	public short[] get_moves()
	{
		return Arrays.copyOf(_moves, _moves.length);
	}
	
	public void set_moves(short[] moves)
	{
		_moves = Arrays.copyOf(moves, moves.length);
	}
	
	public byte[] get_pp()
	{
		return Arrays.copyOf(_pp, _pp.length);
	}
	
	public void set_pp(byte[] pp)
	{
		_pp = Arrays.copyOf(pp, pp.length);
	}
}