package application;

import javafx.fxml.FXML;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import save_editing.save_parser;
import save_editing.game_model;
import save_editing.move;
import save_editing.pokemon;
import save_editing.trainer;

public class controller
{
	private Stage		_stage;
	private save_parser	_save_parser;
	private game_model	_game_model;
	
	@FXML private MenuItem menu_save_as;
	
	@FXML private ChoiceBox trainer_gender_select;
	@FXML private TextField trainer_name;
	@FXML private TextField trainer_money;
	@FXML private TextField trainer_id;
	@FXML private TextField secret_id;
	
	@FXML private ChoiceBox team_select;
	
	@FXML private Button apply_team;
	
	@FXML private TextField nickname_text;
	
	@FXML private ChoiceBox pp_up_one;
	@FXML private ChoiceBox pp_up_two;
	@FXML private ChoiceBox pp_up_three;
	@FXML private ChoiceBox pp_up_four;

	@FXML private ChoiceBox move_one;
	@FXML private ChoiceBox move_two;
	@FXML private ChoiceBox move_three;
	@FXML private ChoiceBox move_four;
	
	@FXML
	public void initialize()
	{	
		// Disable the save option until a save is loaded.
		menu_save_as.setDisable(true);
		
		team_select.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
			@Override public void changed(ObservableValue<? extends Number> selected, Number old_selection, Number new_selection)
			{
				if(_game_model == null || new_selection.intValue() < 0)
					return;
				
				_populate_fields_for_pokemon(_game_model.get_lineup().get(new_selection.intValue()));
			}
		});
		
		// Add move names to the move choice boxes.
		for(int i = 0; move.index_to_move_name(i) != null; ++i)
		{
			move_one.getItems().add(move.index_to_move_name(i));
		}
		move_two.getItems().setAll(move_one.getItems());
		move_three.getItems().setAll(move_one.getItems());
		move_four.getItems().setAll(move_one.getItems());
	}
	
	@FXML
	public void open_save_file()
	{
		FileChooser fc = new FileChooser();
		fc.setTitle("Open Save File");
		fc.setInitialDirectory(new File("./"));
		FileChooser.ExtensionFilter ef = new FileChooser.ExtensionFilter("Save files", "*.sav");
		fc.getExtensionFilters().setAll(ef);
		
		File save_file = fc.showOpenDialog(_stage);
		if(save_file != null)
		{
			try
			{
				save_parser new_save_parser = new save_parser();
				new_save_parser.read_and_parse_save_file(save_file);
				menu_save_as.setDisable(false);
				_game_model = new_save_parser.get_game_model();
				_save_parser = new_save_parser;
				_populate_gui_fields();
			}
			catch(save_editing.FileTooBigException|IOException ex)
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("ERROR");
				alert.setHeaderText("Save open failed!");
				alert.setContentText(ex.toString());
				alert.showAndWait();
			}
		}
	}
	
	@FXML
	public void save_as()
	{
		FileChooser fc = new FileChooser();
		fc.setTitle("Save As...");
		fc.setInitialDirectory(new File("./"));
		fc.setInitialFileName("out.sav");
		FileChooser.ExtensionFilter ef = new FileChooser.ExtensionFilter("Save files", "*.sav");
		fc.getExtensionFilters().setAll(ef);
		
		File save_file = fc.showSaveDialog(_stage);
		if(save_file != null)
		{
			try
			{
				_apply_modifications_to_model();
				_save_parser.set_game_model(_game_model);
				_save_parser.write_save_file(save_file);
			}
			catch(IOException ex)
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("ERROR");
				alert.setHeaderText("Save write failed!");
				alert.setContentText(ex.toString());
				alert.showAndWait();
			}
		}
	}
	
	@FXML
	public void apply_team_changes()
	{
		ArrayList<pokemon> lineup = _game_model.get_lineup();
		pokemon p = lineup.get(team_select.getSelectionModel().getSelectedIndex());
			
		move[] moves = p.get_moves();
		moves[0] = new move(move_one.getSelectionModel().getSelectedIndex(), moves[0].get_current_pp(), (byte)pp_up_one.getSelectionModel().getSelectedIndex());
		moves[1] = new move(move_two.getSelectionModel().getSelectedIndex(), moves[1].get_current_pp(), (byte)pp_up_two.getSelectionModel().getSelectedIndex());
		moves[2] = new move(move_three.getSelectionModel().getSelectedIndex(), moves[2].get_current_pp(), (byte)pp_up_three.getSelectionModel().getSelectedIndex());
		moves[3] = new move(move_four.getSelectionModel().getSelectedIndex(), moves[3].get_current_pp(), (byte)pp_up_four.getSelectionModel().getSelectedIndex());
		p.set_moves(moves);
		
		_game_model.set_lineup(lineup);
	}
	
	private void _apply_trainer_changes()
	{
		String name = trainer_name.getText();
		trainer.gender gender = trainer_gender_select.getSelectionModel().getSelectedIndex() == 0 ? trainer.gender.MALE : trainer.gender.FEMALE;
		int id = Integer.parseInt(trainer_id.getText());
		int sid = Integer.parseInt(secret_id.getText());
		int money = Integer.parseInt(trainer_money.getText());
		
		_game_model.set_trainer(new trainer(name, gender, id, sid, money));
	}
	
	private void _apply_modifications_to_model()
	{
		apply_team_changes();
		
		String name = trainer_name.getText();
		trainer.gender gender = trainer_gender_select.getSelectionModel().getSelectedIndex() == 0 ? trainer.gender.MALE : trainer.gender.FEMALE;
		int id = Integer.parseInt(trainer_id.getText());
		int sid = Integer.parseInt(secret_id.getText());
		int money = Integer.parseInt(trainer_money.getText());
		
		_game_model.set_trainer(new trainer(name, gender, id, sid, money));
	}
	
	private void _populate_fields_for_pokemon(pokemon p)
	{
		nickname_text.setText(p.get_nickname());
		move[] moves = p.get_moves();
		
		// Set move names
		move_one.getSelectionModel().select(moves[0].get_index());
		move_two.getSelectionModel().select(moves[1].get_index());
		move_three.getSelectionModel().select(moves[2].get_index());
		move_four.getSelectionModel().select(moves[3].get_index());
		
		// Set PP Bonuses
		pp_up_one.getSelectionModel().select(moves[0].get_pp_bonuses());
		pp_up_two.getSelectionModel().select(moves[1].get_pp_bonuses());
		pp_up_three.getSelectionModel().select(moves[2].get_pp_bonuses());
		pp_up_four.getSelectionModel().select(moves[3].get_pp_bonuses());
	}
	
	private void _populate_trainer_fields()
	{
		trainer t = _game_model.get_trainer();
		trainer_name.setText(t.get_name());
		trainer_id.setText(Integer.toString(t.get_trainer_id()));
		secret_id.setText(Integer.toString(t.get_secret_id()));
		trainer_money.setText(Integer.toString(t.get_money()));
		
		int gender_index = t.get_gender() == trainer.gender.MALE ? 0 : 1;
		trainer_gender_select.getSelectionModel().select(gender_index);
	}
	
	private void _populate_gui_fields()
	{
		_populate_trainer_fields();
		
		ArrayList<pokemon> lineup = _game_model.get_lineup();
		
		team_select.getItems().clear();
		for(int i = 0; i < lineup.size(); ++i)
		{
			team_select.getItems().add(i);
		}
		team_select.getSelectionModel().selectFirst();
	}
	
	public void set_stage(Stage stage)
	{
		_stage = stage;
	}
}