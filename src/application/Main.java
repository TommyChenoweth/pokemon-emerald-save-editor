package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application
{	
	@Override
	public void start(Stage stage)
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("pokemon_emerald_save_editor.fxml"));
			Parent root = (Parent)loader.load();
			
			stage.setTitle("Pokemon Emerald Save Editor");
			stage.setScene(new Scene(root, 590, 400));
			stage.setResizable(false);
			stage.show();
			controller c = (controller)loader.getController();
			c.set_stage(stage);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		Application.launch(Main.class, args);
	}
}
